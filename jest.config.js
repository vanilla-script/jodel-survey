/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  setupFiles: [
    "<rootDir>/tests/env-config.ts"
  ],
  preset: 'ts-jest',
  testEnvironment: 'node',
};