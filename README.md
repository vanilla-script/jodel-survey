# Welcome to the Survey REST Api

This project is made for the coding challange of jodel.
It is a service for creating, reading surveys and save answers for this surveys.
For storing all the resource it will be used a file storage.

## How to setup the service

1. When you are in the root directory of the project start with running the following commend to install all the necessary packages.

```npm install```

2. Next step is copying the `.env.example` to `.env` for using the definet environment variables in coding.

3. The last step is creating the following files as storage:

```
store/survey-answers.json
store/surveys.json
```
You can change the file names if you want but don`t forget to change the paths in .env.

Now you can either start the server with `npm run start:dev`, which you can use for developing because of each time you make changes on the code the server will be restarted by nodemon or you can use `npm run serve`.

## Testing

If you want to run the tests you have to do some steps before.

1. Copying the `.env.test.example` to `.env.test` for using the definet environment variables in coding.

2. Creating the following files as storage:

```
store/test-survey-answers.json
store/test-surveys.json
```
You can change the file names if you want but don`t forget to change the paths in .env.test.

Now you can run tests with `npm run test`;
For your interest each test will clean up the files.

