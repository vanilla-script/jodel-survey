import * as fs from 'fs';
import * as path from 'path';
import { IStore } from '../src/survey/interfaces';

/**
 * Truncate json file store.
 *
 * @param filePath
 * @returns
 */
export async function clear(filePath: string): Promise<void> {
  const resolvedPath = path.resolve(process.env.ROOT_DIR, filePath);
  return new Promise((resolve, reject) => {
    fs.truncate(resolvedPath, 0, (err) => {
      if (err) {
        return reject(new Error('Cannot truncate data store file'));
      }

      return resolve();
    });
  });
}
