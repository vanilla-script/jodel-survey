import * as request from 'supertest';
import app from '../src/app';

/**
 * Sends a get request to the api endpoint, which returns json.
 * Asserts the response status code default is 200.
 *
 * @param path
 * @param status
 * @returns
 */
export async function get(path: string, status: number = 200): Promise<any> {
  const response = await request(app.callback())
        .get(path)
        .set('Accept', 'application/json')
        .expect(status);

  return response.body;
}

/**
 * Sends a post request with a json body to the api endpoint.
 * Asserts the status code 201 for creating a resource.
 *
 * @param path
 * @param body
 * @returns
 */
export async function post(path: string, body: any, status: number = 201): Promise<any> {
  const response = await request(app.callback())
        .post(path)
        .send(body)
        .set('Accept', 'application/json')
        .expect(status);

  return response.body;
}
