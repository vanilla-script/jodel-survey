import { DateTime } from 'luxon';

/**
 * Assert that the api response error has the right data structure and information about the expected error.
 *
 * @param status
 * @param code
 * @param response
 */
export function assertApiError(status: number, code: string, response: any): void {
  const { code: errorCode, status: errorStatus } = response.error;

  expect({
    code,
    status,
  }).toMatchObject({
    code: errorCode,
    status: errorStatus,
  });
}

/**
 * Assert that the created at property is a valid iso 8601 string, which is created between the last second and now.
 *
 * @param storedDateTime
 */
export function assertCreatedAtProperty(storedDateTime: string): void {
  expect(DateTime.fromISO(storedDateTime).isValid).toBeTruthy();
  expect(DateTime.now() > DateTime.fromISO(storedDateTime)).toBeTruthy();
  expect(DateTime.now().minus({ second: 1 }) < DateTime.fromISO(storedDateTime)).toBeTruthy();
}
