import * as dotenv from 'dotenv';
import app from './app';

// loads all definitions in .env as environment variable.
dotenv.config();

const PORT: number = Number(process.env.PORT) || 3000;
app.listen(PORT);
