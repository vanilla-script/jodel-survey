import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import errorHandler from './middlewares/error-handler';
import responseFormatter from './middlewares/response-formatter';

import surveyRouter from './survey/router';

const app: Koa = new Koa();

// add middlewares
// errorHandler has to be the first one for getting noticed for each error on the application.
app.use(errorHandler)
  .use(responseFormatter)
  .use(bodyParser());

// add endpoints of survey module.
app
  .use(surveyRouter.routes())
  .use(surveyRouter.allowedMethods());

// We dont want to log the explicit error during jest tests because of it slows the tests down.
if (process.env.NODE_ENV !== 'test') {
  // Application error logging.
  app.on('error', console.error);
}

export default app;
