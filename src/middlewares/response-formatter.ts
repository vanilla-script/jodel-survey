import * as Koa from 'koa';

/**
 * Format success response body and put the returned content under the property "data".
 *
 * @param ctx
 * @param next
 */
async function responseFormatterMiddleware(ctx: Koa.Context, next: () => Promise<any>) {
  await next();
  ctx.body = {
    data: ctx.body,
  };
}

export default responseFormatterMiddleware;
