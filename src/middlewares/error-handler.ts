import * as Koa from 'koa';
import { STATUS_CODES } from 'http';

/**
 * Middleware for managing errors, which will throw back to the client.
 * Errors that cause on user mistakes are always of the type ApiException.
 * @param ctx
 * @param next
 */
async function errorHandler(ctx: Koa.Context, next: () => Promise<any>) {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || STATUS_CODES.INTERNAL_SERVER_ERROR;
    ctx.body = {
      error: {
        status: ctx.status,
        code: err.code,
        message: err.message,
      },
    };
    ctx.app.emit('error', err, ctx);
  }
}

export default errorHandler;
