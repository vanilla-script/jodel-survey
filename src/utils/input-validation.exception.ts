import ApiException from './api.exception';
import * as Joi from 'joi';

/**
 * You can use this exception for notifying the client, when a validation error is occured during the input validation.
 */
export default class InputValidationException extends ApiException {
  constructor(err: Joi.ValidationError) {
    super(400, 'input_validation_error', err.details[0].message);
  }
}
