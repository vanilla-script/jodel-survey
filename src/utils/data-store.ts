import * as fs from 'fs';
import * as path from 'path';
import { IStoreItem, IStore } from '../survey/interfaces';

/**
 * Reads asynchron a json file with utf8 characterset and returns the json.
 * The structure of the json file has to be an object.
 *
 * @param filePath
 * @returns Promise
 */
export async function read(filePath: string): Promise<IStore> {
  return new Promise((resolve, reject) => {
    fs.readFile(
      path.resolve(process.env.ROOT_DIR, filePath),
      'utf8',
      (err, data) => {
        if (err) {
          return reject(new Error('Cannot read data from the store.'));
        }

        return resolve(data ? JSON.parse(data) : {});
      });
  });
}

/**
 * Writes asynchron a new entry to the json file.
 * Json file has to be an object, because the content will be added with the id as access property.
 *
 * @param filePath
 * @param content
 * @returns Promise
 */
export async function write(filePath: string, content: IStoreItem): Promise<void> {
  let json = await read(filePath);

  json = {
    ...json,
    [content.id]: content,
  };

  return new Promise((resolve, reject) => {
    fs.writeFile(
      path.resolve(process.env.ROOT_DIR, filePath),
      JSON.stringify(json),
      (err) => {
        if (err) {
          return reject(new Error('Cannot write data to the store.'));
        }

        return resolve();
      });
  });
}
