import { DateTime } from 'luxon';

/**
 * Get an ISO 8601 string of the date time now.
 * @returns string
 */
export function nowISO(): string {
  return DateTime.now().toISO();
}
