import { nanoid } from 'nanoid';

/**
 * Generates a url friendly public id for new data store entries.
 * @returns string
 */
export default (): string => nanoid();
