/**
 * This class should be a super class of all exceptions, which will thrown back to the user as response.
 * 
 */
export default class ApiException extends Error {
  status: number;
  code: string;
  constructor(status: number, code: string, message: string) {
    super(message);
    this.status = status;
    this.code = code;
  }
}
