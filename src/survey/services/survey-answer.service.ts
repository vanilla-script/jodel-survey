import * as DateTime from '../../utils/date-time';
import { IDataStoreClient, ISurvey, ISurveyAnswer, ISurveyAnswerPayload, ISurveyPayload } from '../interfaces';
import publicIdGenerator from '../../utils/public-id-generator';
import * as SurveyService from './survey.service';
import * as repo from '../repositories/survey-answer.repository';
import Joi = require('joi');
import InputValidationException from '../../utils/input-validation.exception';
import SurveyNotFoundException from '../exceptions/survey-not-found.exception';

/**
 * Create a answer for a survey with user input.
 * The user input will be validated before the answer entry will be saved to the data store.
 *
 * @param store
 * @param surveyId
 * @param input
 * @returns
 */
export async function create(store: IDataStoreClient, surveyId: string, input: ISurveyAnswerPayload):
  Promise<ISurveyAnswer> {

  const survey = await SurveyService.findById(store, surveyId);

  if (!survey) {
    throw new SurveyNotFoundException(surveyId);
  }

  await validate(survey, input);

  const surveyAnswer: ISurveyAnswer = {
    ...sanitize(input),
    surveyId,
    id: publicIdGenerator(),
    createdAt: DateTime.nowISO(),
  };

  await repo.create(store, surveyAnswer);

  return surveyAnswer;
}

/**
 * Validate the user input for a survey answer.
 *
 * @param survey
 * @param answer
 */
async function validate(survey: ISurvey, input: ISurveyAnswerPayload) {
  const schema = Joi.object({ answer: Joi.string().valid(...survey.answers).required() });

  try {
    await schema.validateAsync(input);
  } catch (err) {
    throw new InputValidationException(err);
  }
}

/**
 * Cleans the input values for a survey answer.
 *
 * @param input
 * @returns
 */
function sanitize(input: ISurveyAnswerPayload): ISurveyAnswerPayload {
  return {
    answer: input.answer.trim(),
  };
}
