import * as Joi from 'joi';
import * as DateTime from '../../utils/date-time';
import { IDataStoreClient, ISurvey, ISurveyPayload } from '../interfaces';
import publicIdGenerator from '../../utils/public-id-generator';
import * as repo from '../repositories/survey.repository';
import InputValidationException from '../../utils/input-validation.exception';

/**
 * Find a survey in the data store by id.
 *
 * @param store
 * @param surveyId
 * @returns
 */
export async function findById(store: IDataStoreClient, surveyId: string): Promise<ISurvey> {
  return repo.findById(store, surveyId);
}

/**
 * This function manages the creation of a survey.
 * The input will be validated and sanatized for saving the survey in the data store.
 *
 * @param store
 * @param input
 * @returns
 */
export async function create(store: IDataStoreClient, input: ISurveyPayload): Promise<ISurvey> {
  await validate(input);

  const survey: ISurvey = {
    ...sanitize(input),
    id: publicIdGenerator(),
    createdAt: DateTime.nowISO(),
  };

  await repo.create(store, survey);

  return survey;
}

/**
 * Validate the user input fields for a survey.
 * @param input
 */
async function validate(input: ISurveyPayload): Promise<void> {
  const schema = Joi.object({
    question: Joi.string().required(),
    answers: Joi.array().items(Joi.string().required()).min(2).required(),
  });

  try {
    await schema.validateAsync(input);
  } catch (err) {
    throw new InputValidationException(err);
  }
}

/**
 * Cleans the input values for a survey.
 *
 * @param input
 * @returns
 */
function sanitize(input: ISurveyPayload): ISurveyPayload {
  return {
    question: input.question.trim(),
    answers: input.answers.map(answer => answer.trim()),
  };
}
