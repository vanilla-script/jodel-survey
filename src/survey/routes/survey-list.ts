import * as Koa from 'koa';
import * as store from '../../utils/data-store';
import { ISurvey } from '../interfaces';

/**
 * Get a list of all stored surveys in the data store.
 *
 * @param ctx
 */
async function surveylistRoute(ctx: Koa.Context): Promise<void> {
  const surveys = await store.read(process.env.SURVEY_STORE_FILE_PATH);

  ctx.body = Object
    .keys(surveys)
    .map((id) => {
      const survey = surveys[id] as ISurvey;
      return {
        id,
        question: survey.question,
      };
    });
}

export default surveylistRoute;
