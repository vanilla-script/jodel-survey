import * as Koa from 'koa';
import * as SurveyAnswerService from '../services/survey-answer.service';
import * as store from '../../utils/data-store';

/**
 * Create an answer for a survey and save them to the data store.
 *
 * @param ctx
 */
export default async function surveyAnswerRoute(ctx: Koa.Context) {
  const { body } = ctx.request;
  const { surveyId } = ctx.params;

  const surveyAnswer = await SurveyAnswerService.create(store, surveyId, body);

  ctx.status = 201;
  ctx.body = {
    id: surveyAnswer.id,
  };
}
