import * as Koa from 'koa';
import * as SurveyService from '../services/survey.service';
import * as store from '../../utils/data-store';

/**
 * Creates a survey entry in the data store.
 *
 * @param ctx
 */
async function surveyCreateRoute(ctx: Koa.Context) {
  const { body } = ctx.request;

  const survey = await SurveyService.create(store, body);
  ctx.status = 201;
  ctx.body = {
    id: survey.id,
  };
}

export default surveyCreateRoute;
