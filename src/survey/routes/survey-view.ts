import * as Koa from 'koa';
import * as store from '../../utils/data-store';
import SurveyNotFoundException from '../exceptions/survey-not-found.exception';
import { ISurvey } from '../interfaces';

/**
 * Get all information about the requested survey.
 *
 * @param ctx
 */
async function surveyViewRoute(ctx: Koa.Context): Promise<void> {
  const { surveyId } = ctx.params;

  const surveys = await store.read(process.env.SURVEY_STORE_FILE_PATH);

  if (!surveys[surveyId]) {
    throw new SurveyNotFoundException(surveyId);
  }

  const survey = surveys[surveyId] as ISurvey;

  ctx.body = {
    id: survey.id,
    question: survey.question,
    answers: survey.answers,
    createdAt: survey.createdAt,
  };
}

export default surveyViewRoute;
