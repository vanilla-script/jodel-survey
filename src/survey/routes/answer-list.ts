import * as Koa from 'koa';
import * as store from '../../utils/data-store';
import { ISurveyAnswer } from '../interfaces';

/**
 * Get a list of all answers of the requested survey from the data store.
 *
 * @param ctx
 */
async function surveylistRoute(ctx: Koa.Context): Promise<void> {
  const { surveyId } = ctx.params;

  const answers = await store.read(process.env.SURVEY_ANSWER_STORE_FILE_PATH);

  ctx.body = Object
    .keys(answers)
    .filter(id => (answers[id] as ISurveyAnswer).surveyId === surveyId)
    .map((id) => {
      const answer = answers[id] as ISurveyAnswer;
      return {
        id,
        surveyId: answer.surveyId,
        answer: answer.answer,
        createdAt: answer.createdAt,
      };
    });
}

export default surveylistRoute;
