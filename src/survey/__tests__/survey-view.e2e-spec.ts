import { get } from '../../../tests/api';
import { assertApiError, assertCreatedAtProperty } from '../../../tests/assertions';
import { clear as clearStore } from '../../../tests/data-store';
import { IStore, IStoreItem, ISurvey, ISurveyPayload } from '../interfaces';
import * as DateTime from '../../utils/date-time';
import * as Store from '../../utils/data-store';
import publicIdGenerator from '../../utils/public-id-generator';

describe('Get information about a survey as user', () => {
  const publicId = publicIdGenerator();
  const question = 'What kind of food do you like?';
  const answers = ['Fast', 'Veggi', 'Vegan', 'Super'];
  const createdAt = DateTime.nowISO();

  beforeAll(async () => {
    await Store.write(process.env.SURVEY_STORE_FILE_PATH, {
      question,
      answers,
      createdAt,
      id: publicId,
    } as ISurvey);
  });

  afterAll(async () => {
    await clearStore(process.env.SURVEY_STORE_FILE_PATH);
  });

  test('Get information about a survey', async () => {
    const response = await get(`/surveys/${publicId}`);

    expect(response.data).toEqual({
      question,
      answers,
      createdAt,
      id: publicId,
    });
  });

  test('Survey not found error', async () => {
    const randomId = publicIdGenerator();
    const response = await get(`/surveys/${randomId}`, 404);

    assertApiError(404, 'survey_not_found', response);
  });
});
