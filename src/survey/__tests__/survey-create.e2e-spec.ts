import { get, post } from '../../../tests/api';
import { assertApiError, assertCreatedAtProperty } from '../../../tests/assertions';
import * as Store from '../../utils/data-store';
import { clear as clearStore } from '../../../tests/data-store';
import { IStore, IStoreItem, ISurvey, ISurveyPayload } from '../interfaces';

describe('Create a survey as user', () => {
  afterAll(async () => {
    await clearStore(process.env.SURVEY_STORE_FILE_PATH);
  });

  test('Create a survey successful', async () => {
    const payload: ISurveyPayload = {
      question: 'Is this real?',
      answers: ['yes', 'no'],
    };

    const response = await post('/surveys', payload);

    expect(response.data.id).toBeDefined();
  });

  test('Check stored survey', async () => {
    const surveys: IStore = await Store.read(process.env.SURVEY_STORE_FILE_PATH);

    const surveyIds = Object.keys(surveys);
    const survey = surveys[surveyIds[0]] as ISurvey;

    expect(surveyIds).toHaveLength(1);
    expect(survey.id).toBeDefined();
    expect(survey.question).toBe('Is this real?');
    expect(survey.answers).toEqual(['yes', 'no']);

    assertCreatedAtProperty(survey.createdAt);
  });

  test('Check required input fields error', async () => {
    const payloads: ISurveyPayload[] = [
      {
        question: '',
        answers: ['yes', 'no'],
      },
      {
        question: 'What is it?',
        answers: [],
      },
    ];

    for (const payload of payloads) {
      const response = await post('/surveys', payload, 400);

      assertApiError(400, 'input_validation_error', response);
    }
  });

  test('Input field answers at least length 2 error', async () => {
    const payload: ISurveyPayload = {
      question: 'Do we need 2 answers?',
      answers: ['yes'],
    };

    const response = await post('/surveys', payload, 400);

    assertApiError(400, 'input_validation_error', response);

  });
});
