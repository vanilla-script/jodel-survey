import { get } from '../../../tests/api';
import { assertApiError, assertCreatedAtProperty } from '../../../tests/assertions';
import { clear as clearStore } from '../../../tests/data-store';
import { IStore, IStoreItem, ISurvey, ISurveyAnswer, ISurveyPayload } from '../interfaces';
import * as DateTime from '../../utils/date-time';
import * as Store from '../../utils/data-store';
import publicIdGenerator from '../../utils/public-id-generator';

describe('Get a list of survey answers as user', () => {
  const survey = {
    id: publicIdGenerator(),
    question: 'What kind of food do you like?',
    answers: ['Fast', 'Veggi', 'Vegan', 'Super'],
    createdAt: DateTime.nowISO(),
  };

  const answers = [
    {
      id: publicIdGenerator(),
      surveyId: survey.id,
      answer: 'Fast',
      createdAt: DateTime.nowISO(),
    },
    {
      id: publicIdGenerator(),
      surveyId: survey.id,
      answer: 'Vegan',
      createdAt: DateTime.nowISO(),
    },
  ];
  beforeAll(async () => {
    await Store.write(process.env.SURVEY_STORE_FILE_PATH, survey as ISurvey);
    await Store.write(process.env.SURVEY_ANSWER_STORE_FILE_PATH, answers[0] as ISurveyAnswer);
    await Store.write(process.env.SURVEY_ANSWER_STORE_FILE_PATH, answers[1] as ISurveyAnswer);
  });

  afterAll(async () => {
    await clearStore(process.env.SURVEY_STORE_FILE_PATH);
    await clearStore(process.env.SURVEY_ANSWER_STORE_FILE_PATH);
  });

  test('Get list of survey answers', async () => {
    const response = await get(`/surveys/${survey.id}/answers`);

    expect(response.data).toHaveLength(2);
    expect(response.data).toEqual(expect.arrayContaining([
      {
        id: answers[0].id,
        surveyId: survey.id,
        answer: answers[0].answer,
        createdAt: answers[0].createdAt,
      },
      {
        id: answers[1].id,
        surveyId: survey.id,
        answer: answers[1].answer,
        createdAt: answers[1].createdAt,
      },
    ]));
  });

  test('Get empty list', async () => {
    const randomId = publicIdGenerator();
    const response = await get(`/surveys/${randomId}/answers`);

    expect(response.data).toHaveLength(0);
  });
});
