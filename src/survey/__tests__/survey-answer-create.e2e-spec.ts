import { get, post } from '../../../tests/api';
import { assertApiError, assertCreatedAtProperty } from '../../../tests/assertions';
import * as Store from '../../utils/data-store';
import { clear as clearStore } from '../../../tests/data-store';
import { IStore, IStoreItem, ISurvey, ISurveyAnswer, ISurveyAnswerPayload, ISurveyPayload } from '../interfaces';
import * as DateTime from '../../utils/date-time';
import publicIdGenerator from '../../utils/public-id-generator';

describe('Create a survey answer as user', () => {
  const surveyPublicId = publicIdGenerator();
  beforeAll(async () => {
    await Store.write(process.env.SURVEY_STORE_FILE_PATH, {
      id: surveyPublicId,
      question: 'What do you like more?',
      answers: ['Movies', 'Series'],
      createdAt: DateTime.nowISO(),
    } as ISurvey);
  });

  afterAll(async () => {
    await clearStore(process.env.SURVEY_STORE_FILE_PATH);
    await clearStore(process.env.SURVEY_ANSWER_STORE_FILE_PATH);
  });

  test('Create a survey answer successful', async () => {
    const payload: ISurveyAnswerPayload = {
      answer: 'Series',
    };

    const response = await post(`/surveys/${surveyPublicId}/answers`, payload);

    expect(response.data.id).toBeDefined();
  });

  test('Check stored survey answer', async () => {
    const answers: IStore = await Store.read(process.env.SURVEY_ANSWER_STORE_FILE_PATH);

    const answersId = Object.keys(answers);
    const answer = answers[answersId[0]] as ISurveyAnswer;

    expect(answersId).toHaveLength(1);
    expect(answer.id).toBeDefined();
    expect(answer.surveyId).toBe(surveyPublicId);

    assertCreatedAtProperty(answer.createdAt);
  });

  test('Check required input fields', async () => {
    const payloads: ISurveyAnswerPayload[] = [
      {
        answer: '',
      },
    ];

    for (const payload of payloads) {
      const response = await post(`/surveys/${surveyPublicId}/answers`, payload, 400);

      assertApiError(400, 'input_validation_error', response);
    }
  });

  test('Check incorrect answer error', async () => {
    const payload: ISurveyAnswerPayload = {
      answer: 'Wrong answer',
    };
    const response = await post(`/surveys/${surveyPublicId}/answers`, payload, 400);

    assertApiError(400, 'input_validation_error', response);

  });

  test('Survey not found error', async () => {
    const payload: ISurveyAnswerPayload = {
      answer: 'doesnt matter',
    };
    const randomId = publicIdGenerator();
    const response = await post(`/surveys/${randomId}/answers`, payload, 404);

    assertApiError(404, 'survey_not_found', response);

  });
});
