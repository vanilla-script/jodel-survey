import { get } from '../../../tests/api';
import { assertApiError, assertCreatedAtProperty } from '../../../tests/assertions';
import { clear as clearStore } from '../../../tests/data-store';
import { IStore, IStoreItem, ISurvey, ISurveyPayload } from '../interfaces';
import * as DateTime from '../../utils/date-time';
import * as Store from '../../utils/data-store';
import publicIdGenerator from '../../utils/public-id-generator';

describe('Get a list of surveys as user', () => {
  const surveys = [
    {
      id: publicIdGenerator(),
      question: 'What kind of food do you like?',
      answers: ['Fast', 'Veggi', 'Vegan', 'Super'],
      createdAt: DateTime.nowISO(),
    },
    {
      id: publicIdGenerator(),
      question: 'Is this a weird question?',
      answers: ['Sure', 'No way'],
      createdAt: DateTime.nowISO(),
    },
  ];

  beforeAll(async () => {
    await Store.write(process.env.SURVEY_STORE_FILE_PATH, surveys[0] as ISurvey);
    await Store.write(process.env.SURVEY_STORE_FILE_PATH, surveys[1] as ISurvey);
  });

  afterAll(async () => {
    await clearStore(process.env.SURVEY_STORE_FILE_PATH);
  });

  test('Get list of surveys', async () => {
    const response = await get('/surveys');

    expect(response.data).toHaveLength(2);
    expect(response.data).toEqual(expect.arrayContaining([
      {
        id: surveys[0].id,
        question: surveys[0].question,
      },
      {
        id: surveys[1].id,
        question: surveys[1].question,
      },
    ]));
  });
});
