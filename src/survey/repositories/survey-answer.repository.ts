import { IDataStoreClient, ISurveyAnswer } from '../interfaces';

/**
 * Create an answer entry for a survey in the data store.
 *
 * @param store
 * @param surveyAnswer
 */
export async function create(store: IDataStoreClient, surveyAnswer: ISurveyAnswer): Promise<void> {
  await store.write(process.env.SURVEY_ANSWER_STORE_FILE_PATH, surveyAnswer);
}
