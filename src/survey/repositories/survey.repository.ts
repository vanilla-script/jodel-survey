import { IDataStoreClient, ISurvey } from '../interfaces';

/**
 * Create a survey entry in the data store.
 *
 * @param store
 * @param survey
 */
export async function create(store: IDataStoreClient, survey: ISurvey): Promise<void> {
  await store.write(process.env.SURVEY_STORE_FILE_PATH, survey);
}

/**
 * Get a survey from the data store by id.
 *
 * @param store
 * @param surveyId
 * @returns
 */
export async function findById(store: IDataStoreClient, surveyId: string): Promise<ISurvey> {
  const surveys = await store.read(process.env.SURVEY_STORE_FILE_PATH);

  return surveys[surveyId] as ISurvey;
}
