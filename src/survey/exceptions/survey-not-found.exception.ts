import ApiException from '../../utils/api.exception';

/**
 * Use this exception for notifiyng a client that the requested survey was not found.
 */
export default class SurveyNotFoundException extends ApiException {
  constructor(id: string) {
    super(404, 'survey_not_found', `Survey with id: ${id} was not found.`);
  }
}
