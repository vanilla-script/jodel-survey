export interface ISurvey extends IStoreItem {
  id: string;
  question: string;
  answers: string[];
  createdAt: string;
}

export interface ISurveyAnswer extends IStoreItem {
  surveyId: string;
  answer: string;
  createdAt: string;
}

export interface IDataStoreClient {
  write: (filePath: string, content: IStoreItem) => Promise<void>;
  read: (filePath: string) => Promise<IStore>;
}

export interface ISurveyPayload {
  question: string;
  answers: string[];
}

export interface ISurveyAnswerPayload {
  answer: string;
}

export interface IStoreItem {
  id: string;
}

export interface IStore {
  [key: string]: IStoreItem;
}

export interface ISurveyService {
  create: (store: IStore, input: ISurveyPayload) => ISurvey;
}
