import * as Router from 'koa-router';
import listRoute from './routes/survey-list';
import createRoute from './routes/survey-create';
import viewRoute from './routes/survey-view';
import answerCreateRoute from './routes/answer-create';
import answerListRoute from './routes/answer-list';

const routerOpts: Router.IRouterOptions = {
  prefix: '/surveys',
};

const router: Router = new Router(routerOpts);

// domain path for managing surveys
router.get('/', listRoute);
router.get('/:surveyId', viewRoute);
router.post('/', createRoute);

// sub paths for managing answers
router.post('/:surveyId/answers', answerCreateRoute);
router.get('/:surveyId/answers', answerListRoute);

export default router;
